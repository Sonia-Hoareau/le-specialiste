# Liste de Biens Immobiliers

Backend API REST en PHP avec Symfony avec Spring et BDD relationnelle.
Front end avec React Native pour l'Application mobile.
Front end avec React et electron pour l'application desktop (backend).

## Cas d'utilisation

- Créer un compte
	- support OAuth
	- username / password / mail
	- envoi mail confirmation
	- formulaire mot de passe perdu

## Structure de données

```mermaid

classDiagram

class Administrateur {
	int id
	String name
	String firstName
	String email
    String password
}

class BienImmobilier {
	int id
	Date date
	String reference
	Int surface
    Int Number
	String place
    String cp
    String city
    Int yearConstruct
    String description
    Int price
    Int dpe
    Int ges
    Int surfacePlane
    String numberRooms
    Int numberBedrooms
    
}

class Client {
	Int id
	String civility
	String name 
    String firstname
    String telephone
    String email
    
}

class NumberBedroom {
	Int id
	Int number
}

class NumberRoom {
	Int id
	String name
}

class Type {
	Int id
	String name
}

class Photos {
	Int id
	Blob photo
}

Administrateur "1" -- "0,*" BienImmobilier
Client "1" -- "1,*" BienImmobilier
BienImmobilier "1" -- "0,1" NumberBedroom : optionnel
BienImmobilier "1, *" -- "1" Type
BienImmobilier "1" -- "0,1" NumberRoom : optionnel
BienImmobilier "1" -- "0,*" Photos : optionnel

```