// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>hello voici l'appli Le Spécialiste !</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './pages/Home/Home';
import BiensImmobiliers from './pages/BiensImmobiliers/BiensImmobiliers';
import BienImmobilier from './pages/BiensImmobiliers/BienImmobilier';
import Vendre from './pages/Vendre/Vendre';



const App = () =>{

  const Stack = createNativeStackNavigator();

  return (
    
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Biens-immobiliers" component={BiensImmobiliers} />
        {/* Route avec id */}
        <Stack.Screen name="Bien-immobilier" component={BienImmobilier} />
        <Stack.Screen name="Vendre" component={Vendre} />
      </Stack.Navigator>
    </NavigationContainer>

    
  );
}

export default App;