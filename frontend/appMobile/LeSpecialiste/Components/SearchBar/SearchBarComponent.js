import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements/dist/buttons/Button';
// import { SearchBar } from 'react-native-elements';
import { Input } from 'react-native-elements/dist/input/Input';
import tailwind from 'tailwind-rn';

// // Fonction de filtres des biens immobiliers par ville
// const filterBiensImmobiliers = (biensImmobiliers, city) => {
//     return biensImmobiliers.filter(bienImmobilier => bienImmobilier.ville === city);
// }

// input pour rechercher les biens immobiliers par ville
class SearchBarComponent extends React.Component {
    render() {
        return (
            <View style={styles.containerSearch}>
                <Input placeholder='Ville, département, région..' style={tailwind('mt-5 bg-white p-2 text-sm')}/>
                <View style={tailwind('flex-row ')}>
                    <Input placeholder='Surface' style={tailwind('mt-5 bg-white p-2 text-sm')}/>
                    <Input placeholder='Budget' style={tailwind('mt-5 bg-white p-2 text-sm')}/>
                    
                </View>
                <Button title='Rechercher' buttonStyle={tailwind('mt-5 bg-red-100 p-2 text-sm')}/>
            </View>
        );
    }
  
}
const styles = StyleSheet.create({
    containerSearch: {
        maxWidth: '300px'
    }
});
export default SearchBarComponent;
            


