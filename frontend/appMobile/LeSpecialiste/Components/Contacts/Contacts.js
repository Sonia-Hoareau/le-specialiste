import React from "react"
import { View, Text } from 'react-native';
import { Link } from "@react-navigation/native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAt, faPhoneAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons'
import tailwind from 'tailwind-rn';

const Contacts =() =>{
    return(
        <View style={tailwind('mt-4 absolute bottom-4 ')}>
            <Text>
                <Link to="#" style={tailwind('h-8 w-8 py-2')}><FontAwesomeIcon icon={ faEnvelope } style={tailwind(' text-gray-800 py-2')}/></Link>
                <Link to="#"><FontAwesomeIcon icon={ faAt } style={tailwind(' text-gray-800 px-2')}/></Link>
                <Link to="#"><FontAwesomeIcon icon={ faPhoneAlt } style={tailwind('h-8 w-8 text-gray-800 px-2')}/></Link>
                <Link to="#"><FontAwesomeIcon icon={ faFacebookSquare } style={tailwind('h-8 w-8 text-gray-800 px-2')}/></Link>
            </Text>
        </View>
    )
}

export default Contacts;