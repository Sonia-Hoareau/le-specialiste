import axios from "axios";
import cache from "./cache";

async function findAll() {

    const bienImmobilier = await cache.get('bien_immobiliers');
    if (bienImmobilier) return bienImmobilier;
    
    return axios
    // pour emulateur android studio
    // .get('http://localhost:8000/api/bien_immobiliers')
    //  .get('http://10.0.2.2:8000/api/bien_immobiliers')
   .get('http://192.168.1.60:8000/api/bien_immobiliers/')
        .then(response => {
        const bienImmobiliers =  response.data["hydra:member"];
        cache.set('bien_immobiliers', bienImmobiliers);
        return bienImmobiliers;
    });
    
}

async function find(id){
    console.log('result');
    const cacheBienImmobilier = await cache.get('bien_immobiliers.' + id);
    
    if (cacheBienImmobilier) return cacheBienImmobilier;

    return axios
    // .get('http://10.0.2.2:8000/api/bien_immobiliers/' + id)
   .get('http://192.168.1.60:8000/api/bien_immobiliers/' + id)
   
//    .get('http://localhost:8000/api/bien_immobiliers/' + id)
    // { ...bienImmobilier,
    //     id : parseInt(bienImmobilier.id)
    // }

    .then(response => {

        const bienImmobilier = response.data;
        cache.set('bien_immobiliers.' + id, bienImmobilier);
        console.log('result');
        console.log(bienImmobilier);
        return bienImmobilier;
        

    }).catch(err => console.log(err));

        


    
}

export default { 
    findAll,
    find
}