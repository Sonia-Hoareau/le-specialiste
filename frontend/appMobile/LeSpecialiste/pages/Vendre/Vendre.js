import React from "react";
import { View, Text } from 'react-native';
import Contacts from "../../Components/Contacts/Contacts";
import tailwind from 'tailwind-rn';
import StyleSheet from 'react-native-media-query';

class Vendre extends React.Component{
    render(){
        return(
            <>
                <View style={tailwind('mt-5 px-5 ')}>
                    <Text style={tailwind('text-xl text-blue-900')} >Vendez vite et au bon prix !</Text>
                    <Text style={tailwind('text-justify text-sm')}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, ab. Deserunt tenetur ex nostrum voluptates placeat maxime repellendus, debitis quod perspiciatis sit, facilis sapiente reiciendis, eos vitae. Blanditiis, fuga repellendus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur voluptas hic totam quis doloremque delectus repellendus, quibusdam dolore asperiores quae cupiditate amet sit architecto. Mollitia in aliquam magni cum doloremque.</Text>
                    <Text style={tailwind('text-xl mt-4')}>Nous contactez</Text>
                    <Text style={tailwind('text-lg text-blue-600 font-bold')}>03 81 80 00 00</Text>
                    <Text style={tailwind('text-lg text-blue-600 font-bold')}>mail@mail.com</Text>
                    <Text style={tailwind('text-lg text-blue-600 font-bold')}>154, rue du Général Leclerc</Text>
                    <Text style={tailwind('text-lg text-blue-600 font-bold')}>25000 Besançon</Text>
                </View>
                <View style={styles.contact}>
                    <Contacts />
                </View>
               

            </>
        )
    }
}

const {ids, styles} = StyleSheet.create({
    contact: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 'auto',
        
        // aspectRatio: 1 * 1.4,
    
        // width: 100,
        // width: '100vw',
        // marginRight: 'auto',
        // marginLeft: 'auto',
        // maxWidth: '320px',
        // paddingRight: '0px',
        // paddingLeft: '0px'
    
    
    }
    
    
    
  });


export default Vendre;