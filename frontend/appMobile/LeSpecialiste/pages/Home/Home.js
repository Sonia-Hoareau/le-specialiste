import { Link } from "@react-navigation/native";
import React from "react";
import { View, Text, Image, ImageBackground } from 'react-native';
import tailwind from 'tailwind-rn';
import Contacts from "../../Components/Contacts/Contacts";
import StyleSheet from 'react-native-media-query';
// import SearchBarComponent from "../../Components/SearchBar/SearchBarComponent";

class Home extends React.Component{
    render(){
        return(
            <View style={styles.container} >
                <View><Image source={require('../../assets/logo-le-specialiste.png')} style={styles.image}/></View>
                <ImageBackground source={require('../../assets/bg-home.png')} style={styles.imageBackground} >
                <View style={tailwind('mt-4 px-4 pt-6 pb-16 ')}>
                    <Text style={tailwind('text-white text-2xl font-semibold text-center ')}>Vous souhaitez acheter  un bien Immobilier, 3 étapes</Text>
                    <Text style={tailwind('text-white text-lg text-center mt-1 leading-6')}>une recherche simple,  vous nous contactez, et on s'occupe de vous !</Text>
                    <View style={tailwind('flex-row justify-between flex-wrap')}> 
                        <View style={tailwind('mt-5 bg-red-500 px-7  py-1')}><Link to="/Biens-immobiliers" style={tailwind('text-white text-lg font-semibold')}>Acheter</Link></View>
                        <View style={tailwind('mt-5 bg-blue-200 px-7  py-1')}><Link to="/Vendre" style={tailwind('text-blue-900 text-lg font-semibold')}>Vendre</Link></View>
                        {/* <SearchBarComponent/> */}
                    </View>
                    
                   
                </View>
                </ImageBackground>
                
                <Contacts/>
            </View>
        )
    }
}

// const styles = StyleSheet.create({
//     container: {
//         height: '100%',
//         alignItems: 'center',
//         paddingTop: 10,
//         // maxWidth: '375px',
//         marginRight: 'auto',
//         marginLeft: 'auto',
//         width: '100vw',
//         paddingRight: '0px',
//         paddingLeft: '0px'
    
    
//     },
//     image: {
//         width: 200,
//         height: 60,
//     },
//     // imageBackground: {
//     //     width: '100vw',
//     //     minHeight: '400px',
//     //     height: '500px',
//     //     zIndex: -1,
//     //     marginRight: 'auto',
//     //     marginLeft: 'auto',
//     //     marginTop: 10,
//     //     '@media(max-width: 568px)' :{
//     //         marginTop:0,
//     //         height: '250px',
//     //     }
//     }
   
//   });
  const {ids, styles} = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        // aspectRatio: 1 * 1.4,
        paddingTop: 10,
        height: '100%',
        // width: 100,
        // width: '100vw',
        // marginRight: 'auto',
        // marginLeft: 'auto',
        // maxWidth: '320px',
        // paddingRight: '0px',
        // paddingLeft: '0px'
    
    
    },
    imageBackground: {
        width: 300,
        height: 'auto',
        // height: '80%',
        // minWdth: 300,
        // minHeight: 320,
        // maxHeight:340,
        // maxWidth: 350,
        overflow: 'hidden',
        borderRadius: 30,
        // zIndex: -1,
        // marginRight: 'auto',
        // marginLeft: 'auto',
        marginTop:10,
        recizeMode: 'cover',
        // borderRadius: 20,
        // backgroundSize: 'cover',
        // '@media(min-width: 600px)' :{
        //     marginTop:30,
        //     height: '250px',
        // }
    },
    image: {
        width: 200,
        height: 60,
        // recizeMode: 'center',
        
    },
    
    
  });


export default Home;