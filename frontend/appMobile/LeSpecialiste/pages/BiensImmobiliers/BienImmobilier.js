import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Link } from "@react-navigation/native";
import BiensImmobiliersAPI from "../../services/BiensImmobiliersAPI";
import tailwind from 'tailwind-rn';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons'
import StyleSheet from 'react-native-media-query';
// import FormatedNumber from '../../Components/FormatefNumber/FormatedNumber';


const BienImmobilier = ({route, navigation}) => {
    const id = route.params.id;
    // récupérer les biens immobiliers
    const [bienImmobilier, setBienImmobilier] = useState([]);
    const [fetchAwait, setFetchAwait] = useState(true);
    const fetchBienImmobilier = async () => {
        try {
            const data = await BiensImmobiliersAPI.find(id);
            setBienImmobilier(data);
            setFetchAwait(false);

        } catch (error) {
            // console.log(error.response);
            console.log(error);
        
        }
    };
    
    // Récupération des BiensImmobiliers à chaque chargement du composant
    useEffect(() => {
        setFetchAwait(true);
        fetchBienImmobilier();
        
    }, []);

    // image
    // useEffect(() => {
    //     fetchImage();
    // }, [bienImmobilier]);

    // const imageBien = bienImmobilier.image;
        return(
            
            <ScrollView style={tailwind('p-3')}>

                <View style={tailwind('flex flex-row justify-between items-end  mt-2 mb-4 mx-3')}>
                    <Image source={require('../../assets/logo-le-specialiste.png')} style={styles.image}/>
                    <Link to="#"><FontAwesomeIcon icon={ faBars } style={tailwind('h-8 w-8 text-gray-800 px-2')}/></Link>
                </View>
                
                <View style={tailwind('bg-blue-900')}>
                    <Text style={tailwind('text-2xl text-center text-white py-3')}> {bienImmobilier.type} {bienImmobilier.numberRoom}</Text>
                </View>
                
                {fetchAwait === false ? 
                    (bienImmobilier.images.map(
                        (photo, index) => {
                            
                                 return <Image key={index} source={{uri:'http://192.168.1.60:8000/' + "uploads/images/" + photo.filePath}} style={styles.imageBiensImmobilier}/>
                                // return <Image source={'http://10.0.2.2:8000/' + "uploads/images/" + photo.filePath} style={styles.imageBiensImmobilier}/>
                        }
                )) : null}

                <View style={tailwind('px-2 pb-6')} >
                    {/* <Image source={'http://localhost:8000/' + "uploads/images/" + bienImmobilier.image} style={styles.imageBiensImmobilier}/> */}
                    <Text style={tailwind('px-1 uppercase')}>réf : {bienImmobilier.reference}</Text>
                    <Text style={tailwind('flex px-1 text-base')}>{bienImmobilier.surface} m<Text style={tailwind('text-xs leading-5')}>2</Text> </Text>
                    
                    <View style={tailwind('px-1 flex-row')}>
                        <Text style={tailwind('text-xl')}>{bienImmobilier.cp} - </Text>
                        <Text style={tailwind('text-xl')}>{bienImmobilier.city}</Text>
                    </View>
                    
                    
                    <View style={tailwind('container flex-row  mt-4')}> 
                        <View style={tailwind('bg-gray-800 px-4 py-2 w-5/12')}>
                            <Text style={tailwind('text-base text-white')}>{bienImmobilier.price}</Text> 
                        </View>
                        <View style={tailwind('bg-gray-600 px-3 py-2 w-1/12 ')}>
                            <Text style={tailwind('text-lg text-white')}>€</Text>
                        </View>
                    </View>
                    
                    <Text style={tailwind('text-xl border-b-2 border-blue-900 py-2 text-blue-900 mt-2')}> Description : </Text>
                    <Text style={tailwind('text-base py-2')}>{bienImmobilier.description} </Text>
                    
                    <Text style={tailwind('text-xl border-b-2 border-blue-900 py-2 text-blue-900')}> Détails : </Text>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1 ')}>Type de bien :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.type} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Année de construction :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.yearConstruct} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Localisation :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.cp} </Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.city} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Surface habitable :</Text>
                        <Text style={tailwind('text-lg font-bold py-1 flex px-1')}> {bienImmobilier.surface} m<Text style={tailwind('text-xs leading-5')}>2</Text></Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Prix :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.price}  </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Nombre de pièc(s) :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.numberRoom} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>Accés extérieur :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.surfacePlane} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>DPE :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.dpe} </Text>
                    </View>
                    <View style={tailwind('flex-row items-center')}>
                        <Text style={tailwind('text-sm py-1')}>GES :</Text>
                        <Text style={tailwind('text-lg font-bold py-1')}> {bienImmobilier.ges} </Text>
                    </View>
               
                </View>
            </ScrollView>
        )
    }
const {ids, styles} = StyleSheet.create({

    image: {
        width: 200,
        height: 60,
    },
    imageBiensImmobilier: {
        width: '100%',
        height: 190,
        marginBottom: 10,
    },
    
});


export default BienImmobilier;