import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image} from 'react-native';
import BiensImmobiliersAPI from "../../services/BiensImmobiliersAPI";
import tailwind from 'tailwind-rn';
import { Link } from "@react-navigation/native";
import StyleSheet from 'react-native-media-query';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBars, faCaretRight } from '@fortawesome/free-solid-svg-icons';
// import FormatedNumber from '../../Components/FormatefNumber/FormatedNumber';

const BiensImmobiliers = (props ) => {
    
    // récupérer les biens immobiliers
    const [biensImmobiliers, setBiensImmobiliers] = useState([]);
    const fetchBienImmobilier = async () => {
        try {
            const data = await BiensImmobiliersAPI.findAll();
            setBiensImmobiliers(data);
        } catch (error) {
            console.log(error.response);
        
        }
    };
    // Récupération des BiensImmobiliers à chaque chargement du composant
    useEffect(() => {
        fetchBienImmobilier();
    }, []);
   

    


        return(
            // liste des biens immobiliers avec flatlist
            <View style={styles.container}>
                <View style={tailwind('flex flex-row justify-between items-end  mt-2 mb-4 mx-3')}>
                    <Image source={require('../../assets/logo-le-specialiste.png')} style={styles.image}/>
                    <Link to="#"><FontAwesomeIcon icon={ faBars } style={tailwind('h-8 w-8 text-gray-800 px-2')}/></Link>
                </View>
                <View style={tailwind('bg-blue-900 px-4')}>
                    
                    <Text style={tailwind('uppercase text-2xl font-semibold text-center text-white mt-4')}>Tous nos biens</Text>
                    <Text style={tailwind('text-white text-lg mt-1  mb-4')}>35 biens trouvés</Text>
                    
                    <FlatList 
                        data={biensImmobiliers}
                        key={(item) => item.id}

                        // mettre un lien vers le détail du bien immobilier
                        // onPress={() => props.navigation.navigate("BienImmobilierDetails", {bienImmobilierId: item._id})}
                        renderItem={({item}) => (

                            <View style={tailwind('')}>

                                 <View style={tailwind('z-10 absolute right-2 top-44 flex-row')}> 
                                    <View style={tailwind('bg-gray-800 px-3 py-1')}>
                                        <Text style={tailwind('text-base text-white')}>{item.price}</Text>
                                      
                                        
                                    </View>
                                    <View style={tailwind('bg-gray-600 px-3 py-1')}>
                                        <Text style={tailwind('text-base text-white')}>€</Text>
                                    </View>
                                </View>

                                {/* afficher la première image du tableau */}
                                {item.images.filter((v,i)=>i==0).map(
                    
                                    (photo , index) => {
                                        if( index===0 ){ 
                                            // return <Image source={'http://localhost:8000/' + "uploads/images/" + photo.filePath} style={styles.imageBiensImmobilier}/>
                                            //  return <Image source={'http://10.0.2.2:8000/' + "uploads/images/" + photo.filePath} style={styles.imageBiensImmobilier}/>
                                             return <Image key={index} source={{uri:'http://192.168.1.60:8000/' + "uploads/images/" + photo.filePath}} style={styles.imageBiensImmobilier}/>
                                            
                                        }
                                    }
                                )}  
                               
                    
                                
                               
                                <View style={tailwind('bg-white px-2 py-2 pb-7 mb-9')}> 
                                   
                                    <Text style={tailwind('text-xl font-semibold mt-4')}> {item.type} {item.numberRoom}</Text>
                            
                                    
                                    <Text style={tailwind('flex px-1 text-base mt-2')}>{item.surface} m<Text style={tailwind('text-xs ')}>2</Text> </Text>
                                    
                                    <View style={tailwind('px-1 flex-row')}>
                                        <Text style={tailwind('text-base')}>{item.cp} - </Text>
                                        <Text style={tailwind('text-base')}>{item.city}</Text>
                                    </View>
                                    
                                    <View style={tailwind('absolute right-2 top-28 bg-red-500 px-2 py-1')}>
                                        <Link to={{ screen: "Bien-immobilier", params: {id: item.id }}} style={tailwind('flex text-white text-right text-lg ')}>Voir plus <FontAwesomeIcon icon={ faCaretRight } style={tailwind('h-6 w-6 text-white')}/></Link>
                                    </View>
                                        
                                </View>
                            </View>
                        )}
                    />
                

                </View>
            </View>
        );
}

const {ids, styles} = StyleSheet.create({
    
    container: {
        // flex: 1,
        // marginTop: 10,
        // width: 100,
        // overflow: 'hidden',
    },
    imageBiensImmobilier: {
        width: '100%',
        height: 190,
    },
    image: {
        width: 200,
        height: 60,
    },
    
});

export default BiensImmobiliers