module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        rougeSpecialiste: "#FF5050", 
        rougeFonceSpecialiste: "#AF0000",
        bleuSpecialiste: "#004F8A",
        bleuLightSpecialiste: "#4780AB",
        bleuCielSpecialiste: "#BEF6FF",
        grisFondImputSpecialiste: "#E8EFF5",
        grisIconeSpecialiste: "#999999",
        grisFonceSpecialiste: "#333333"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    
  ],
  
  
  
}