import React  from 'react'
// import BgConnexion from './bg-connexion.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt} from '@fortawesome/free-solid-svg-icons';
import BtnConnexion from './SubComponents/BtnConnexion';
// import { useForm } from "react-hook-form";
import content from '../static';
// import { yupResolver } from '@hookform/resolvers/yup';
// import * as yup from 'yup';
import AuthAPI from '../../../services/AuthAPI';
import { toast } from 'react-toastify';

AuthAPI.setup();

const Connexion  = (props) => {
    // validation du formulaire front
    // const { register, formState: { errors } } = useForm({
    //     resolver: yupResolver(schema),
    // });
    // const {  handleSubmit: { errors } } = useForm({
    //     resolver: yupResolver(schema),
    // });

    // const onSubmit = (data) => console.log(data);
    // console.log(errors);
    
    
    // lien api
    const [credentials, setCredentials] = React.useState({
        username: '',
        password: ''
    });

    const [error, setError] = React.useState('');

    // Gestion des champs
    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;
        
        setCredentials({...credentials, [name]: value});
    }
    const login = () => {
        props.login();
    }

    // Gestion du submit
    const handleSubmit = async event => {
        event.preventDefault();
        // mettre un loader
        try{
            const auth = await AuthAPI.authenticate(credentials);
            // appel de l'authentification api
            // await AuthAPI.authenticate(credentials);
        
            // redirection vers la page admin
           if(auth){
               login();
               toast.success("Vous êtes connecté !");
            }else{
                setError("Aucun compte ne correspond à ces informations");
                toast.error("Une erreur est survenue");
            }


            // dirige vers la page admin(base)
            // window.location.href = '/#admin';
            setError('');

        // test récupération liste biens immobiliers
        //    const data = await BienImmobiliersAPI.findAll();
        //    console.log(data);

        }catch(error){
            setError("Aucun compte ne possède cette adresse email");
            toast.error("Une erreur est survenue");
        }
       
    };
    

    return (
        <div className="flex justify-center relative lg:pl-14 mt-4">
            <div className='bg-image bg-contain bg-no-repeat'>

            
            {/* <img src={ BgConnexion } className="h-100" alt="logo" /> */}
            

            <div className="text-white text-center p-8 pt-6 mr-2 md:mr-8">
                <span className="md:text-6xl text-4xl"><FontAwesomeIcon icon={ faSignInAlt }/></span>
                <p className="text-3xl font-medium mb-2">Connexion</p>
            
                <form onSubmit={handleSubmit} >
                    {content.inputs.map((input, key) => {
                        return (
                            <div key={key}>
                                <input value={credentials[input.value]} onChange={handleChange} name={input.name} placeholder={input.placeholder} type={input.type}  className="md:p-3 p-2 mb-1 my-5 min-w-full max-w-xs md:text-lg text-black" />
                                
                                {/* <p className="text-red-400 font-medium">{errors[input.name]?.message}</p> */}
                                {/* if(error){
                                    <p className="text-red-400 font-medium">{error}</p>
                                } */}
                                {error && <p className="text-red-400 font-medium">{error}</p>}
                                {/* {error && <p>{error}</p>} */}
                                
                            </div>
                            
                        );
                    })}
                    <div>
                        <p className="font-bold italic mt-3 min-w-full text-base mb-3"><a href="/home">Vous avez oublié votre mot de passe ?</a></p>
                    </div>
                    
                    <BtnConnexion/>
                    
                </form>
            </div>
            </div>
        </div> 
    );
  }

  export default Connexion ;