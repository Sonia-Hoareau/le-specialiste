import React from "react";

const Copyright = props =>{
    return(
        <p className="flex text-center justify-center md:mt-12 mt-4">© 2022 - <span className="text-bleuSpecialiste">Site réalisé par <span className="font-semibold">Sonia Hoareau</span></span> </p>
    )
}

export default Copyright;