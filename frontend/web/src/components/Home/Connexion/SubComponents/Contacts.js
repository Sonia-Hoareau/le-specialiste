import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAt, faPhoneAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons'

const Contacts = props =>{
    return(
        <div className="flex mt-4 justify-center mb-16">
            <p className="text-3xl px-2 text-gray-700 hover:text-bleuSpecialiste"><a href="#"><FontAwesomeIcon icon={ faEnvelope }/></a></p>
            <p className="text-3xl px-2 text-gray-700 hover:text-bleuSpecialiste"><a href="#"><FontAwesomeIcon icon={ faAt }/></a></p>
            <p className="text-3xl px-2 text-gray-700 hover:text-bleuSpecialiste"><a href="#"><FontAwesomeIcon icon={ faPhoneAlt }/></a></p>
            <p className="text-3xl px-2 text-gray-700 hover:text-bleuSpecialiste"><a href="#"><FontAwesomeIcon icon={ faFacebookSquare } className=""/></a></p>
        </div>
    )
}

export default Contacts;