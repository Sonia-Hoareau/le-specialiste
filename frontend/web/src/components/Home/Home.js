import React from 'react';
import '../../index.css';
import logoLeSpecialiste from './logoLeSpecialiste.svg';
import TextAccueil from './TextAccueil/TextAccueil';
import Connexion from './Connexion/Connexion';
import Copyright from './Connexion/SubComponents/Copyright';


const Home = (props ) =>{

   const login = () =>{
       props.login();
   }

    return(
        <div className="p-5 bg-white container mx-auto">
            <header className="App-header ">
                <h1><img src={logoLeSpecialiste} className="h-20 mx-auto" alt="logo" /></h1>
            </header>
      
            <div className="md:flex justify-center mt-6">
                <TextAccueil />
                <Connexion  login={() => login()}/>
                
            </div>
            <Copyright/>
            
        </div>
        
    );
}
    


export default Home