const content = {
    inputs:[
        {
            name: 'username',
            placeholder: 'Email',
            type: 'email',
            value: 'username'
            
            // htmlFor: 'username',
            // id: 'username'
        },
        {
            name: 'password',
            placeholder: 'Mot de passe',
            type: 'password',
            value: 'password'
            
            // htmlFor: 'password',
            // id: 'password'
        },
        // {
        //     name: 'passwordConfirm',
        //     placeholder: 'Confirmer votre mot de passe',
        //     type: 'password'
        // },
        
    ]
};

export default content;