import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAt, faPhoneAlt } from '@fortawesome/free-solid-svg-icons'

const TextAccueil = ( ) =>{
    return(
        <div className="md:border-l-2 md:border-black md:pl-10 md:mt-10 md:pr-14">
            <div className="md:w-96">
                <h1 className="Text-accueil text-black md:text-3xl lg:text-4xl md:text-left text-center uppercase font-thin md:mt-10">L'<span className="font-medium">accès</span> au service le spécialiste <br/> <span className="font-medium">est réservé aux administrateurs</span>.</h1>
                <div className='contact-home'>
                    <h2 className="mt-5 lg:mt-20 md:text-2xl font-medium uppercase ">Nous contacter</h2>
                    <p className="md:text-2xl font-normal mt-4"><FontAwesomeIcon icon={ faPhoneAlt }/> 03 81 80 78 96</p>
                    <p className="md:text-2xl font-normal mt-2"><FontAwesomeIcon icon={ faAt }/> mail@mail.com</p>
                </div>
            </div>
            
        </div>
         
    )
}

export default TextAccueil