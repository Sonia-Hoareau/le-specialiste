import React  from 'react';
import '../../index.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPowerOff } from '@fortawesome/free-solid-svg-icons'
import AuthAPI from '../../services/AuthAPI';
import { toast } from 'react-toastify';

//   const [isConnected, setIsConnected] = useState(false);

const Logout = (props) =>{

    const handleLogout = () => {
        AuthAPI.logout();
        toast.info("Vous êtes déconnecté 😀");
        props.logout();
    }

    return(
        // <button onClick={ handleLogout } onLogout={setIsConnected} className="logout-button text-red-600 mt-5" redirect to="/">Déconnexion
        <button onClick={ handleLogout }  className="logout-button text-rougeSpecialiste hover:text-rougeFonceSpecialiste mt-5" redirect to="/">Déconnexion
            <span className="text-1xl ml-2 "><FontAwesomeIcon icon={ faPowerOff } /></span>
        </button>
        
    )
}

    


export default Logout