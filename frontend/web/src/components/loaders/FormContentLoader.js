import React from 'react'
import ContentLoader from 'react-content-loader'

const FormContentLoader = props => (
    <ContentLoader className="mx-auto"
        viewBox="0 0 500 160"
        height={160}
        width={400}
        backgroundColor="#4780AB"
        {...props}
    >
        <circle cx="150" cy="86" r="8" />
        <circle cx="194" cy="86" r="8" />
        <circle cx="238" cy="86" r="8" />
  </ContentLoader>
)

FormContentLoader.metadata = {
  name: 'DaniloWoz',
  github: 'danilowoz',
  description: 'Nested list',
  filename: 'NestedList',
}

export default FormContentLoader