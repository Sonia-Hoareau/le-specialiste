import axios from "axios";
import Cache from "./cache";
import { BIENS_IMMOBILIERS_API } from "../configs/url";

import PhotosAPI from "./PhotosAPI";

async function findAll() {

    const cacheBiensImmobiliers = await Cache.get('bien_immobiliers');
    if (cacheBiensImmobiliers) return cacheBiensImmobiliers;

    return axios
    .get(BIENS_IMMOBILIERS_API)
    .then(response => {
        const biensImmobiliers =  response.data["hydra:member"];
        Cache.set('bien_immobiliers', biensImmobiliers);
        return biensImmobiliers;
    });
}

async function find(id){
    const cacheBienImmobilier = await Cache.get('bien_immobiliers.' + id);

    if (cacheBienImmobilier) return cacheBienImmobilier;

    return axios
    .get(BIENS_IMMOBILIERS_API + '/' + id)
    .then(response => {

        const bienImmobilier = response.data;
        Cache.set('bien_immobiliers.' + id, bienImmobilier);
        return bienImmobilier;
    });
}

function create(bienImmobilier) {
    return axios
    .post(BIENS_IMMOBILIERS_API,
    { ...bienImmobilier, 
        client: `/api/clients/${parseInt(bienImmobilier.client)}`,
        dpe: parseInt(bienImmobilier.dpe), 
        ges: parseInt(bienImmobilier.ges), 
        price: parseInt(bienImmobilier.price), 
        surface: parseInt(bienImmobilier.surface),
        yearConstruct: parseInt(bienImmobilier.yearConstruct),
        surfacePlane: parseInt(bienImmobilier.surfacePlane),
        numberBedroom: parseInt(bienImmobilier.numberBedroom),
       
    }).then(async response => {
        const cacheBiensImmobiliers = await Cache.get('bien_immobiliers');
            if(cacheBiensImmobiliers){
                Cache.set('bien_immobiliers', [ ...cacheBiensImmobiliers, response.data]);
            }
            if(bienImmobilier.photos){
            PhotosAPI.create(bienImmobilier, response.data['id']);
        }
            return response;
            
    });
}

function deleteBiensImmobilier(id, bienImmobilier) {
    return axios
        // on envoie une requête DELETE à l'api
        .delete(BIENS_IMMOBILIERS_API + '/' + id)   
        .then(async response => {
            const cacheBiensImmobiliers = await Cache.get('bien_immobiliers');
            if(cacheBiensImmobiliers){
                Cache.set('bien_immobiliers', cacheBiensImmobiliers.filter(b => b.id !== id));
            }
            // PhotosAPI.deletePhoto(bienImmobilier, response.data);
            return response;
        });
}

function update(id, bienImmobilier, client) {
    return axios
    .put(BIENS_IMMOBILIERS_API + '/' + id, 
    { ...bienImmobilier, 
    client: `/api/clients/${bienImmobilier.client}` ,
        dpe: parseInt(bienImmobilier.dpe), 
        ges: parseInt(bienImmobilier.ges), 
        price: parseInt(bienImmobilier.price), 
        surface: parseInt(bienImmobilier.surface),
        yearConstruct: parseInt(bienImmobilier.yearConstruct),
        surfacePlane: parseInt(bienImmobilier.surfacePlane),
        numberBedroom: parseInt(bienImmobilier.numberBedroom),
    }).then(async response => {
        
        const cacheClient = await Cache.get('clients.' + client +id);
        const cacheBiensImmobiliers = await Cache.get('bien_immobiliers');
        
        if(cacheClient){
            const indexx = cacheClient.findIndex(b => b.id === +id);
            cacheClient[indexx] = response.data;
        }
        
        if(cacheBiensImmobiliers){
            const index = cacheBiensImmobiliers.findIndex(b => b.id === +id);
            cacheBiensImmobiliers[index] = response.data;      
        }
        if(bienImmobilier.photos){
            PhotosAPI.create(bienImmobilier, response.data['id']);
        }

        return response;
    });
    
}

export default { 
    findAll,
    find,
    create,
    update,
    delete: deleteBiensImmobilier,
};