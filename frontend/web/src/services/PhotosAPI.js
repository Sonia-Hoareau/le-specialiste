import axios from "axios";
import { PHOTOS_API } from "../configs/url";

function findAll() {
    return axios
    .get(PHOTOS_API)
    .then(response => response.data['hydra:member']);
}

function create(bienImmobilier, id) {
    let data = new FormData();
    data.append('file', bienImmobilier.photos);
    data.append('bienImmobilier', id);
    return axios({
        method: "post",
        url: PHOTOS_API,
        data: data,
        headers: {
           "Content-Type": "multipart/form-data"
        },
        
    }).then(function(response) {
        //handle success
        console.log(response);
     })
     .catch(function(response) {
        //handle error
        console.log(response);
     });
}
// // delete une photo dans le bien immobilier
// async function deletePhoto(id) {
//     // return axios
//     // .delete(`${PHOTOS_API}/${id}`)
//     // .delete(PHOTOS_API + '/' + id)
//     return axios({
//         method: "delete",
//         url: PHOTOS_API + '/' + id,
//         headers: {
//            "Content-Type": "multipart/form-data"
//         },
        
//     })
//     .then(response => response.data);
    
// }

export default { 
    findAll,
    create
};