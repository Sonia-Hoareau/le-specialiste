import axios from "axios";  
import jwtDecode from 'jwt-decode';
import { LOGIN_API } from "../configs/url";

// fonction deconnexion
function logout(){
    // on supprime le token dans le localstorage
    window.localStorage.removeItem('authToken');
    // on supprime le header d'autorisation
    delete axios.defaults.headers['Authorization'];
    console.log(logout);
    
}

// Fonction de connexion
function authenticate(credentials){
    return axios
        // appel en post pour authentifier en passant les credentials
        .post(LOGIN_API, credentials)
        //récupère le token
        .then(response => response.data.token)
        .then(token => {
            // stockage du token dans le localstorage
            window.localStorage.setItem('authToken', token);

            // on prévient axios qu'on a un header avec bearer par défaut sur les requêtes HTTP
            setAxiosToken(token);

            // on retourne vrai si le token est valide
            return true;
           
    
        }).catch(error => {
            // on retourne faux si le token est invalide)
            return false;
        });
        
}

function setAxiosToken(token){
    axios.defaults.headers['Authorization'] = "Bearer " + token;
}

// fonction qui charge le token 
function setup() {
    // 1. voir si on a un token ?
    const token = window.localStorage.getItem('authToken');
    // 2. Si le token est encore valide
    if(token) {
      // 3. Decode le token
      const {exp: expiration} = jwtDecode(token);
        if(expiration * 10000 > new Date().getTime()) {
            setAxiosToken(token);
        }else{
            logout();
            
        }
    }else{
        logout();
        
    }
}

function isConnected(){
    const token = window.localStorage.getItem('authToken');
    if(token) {
        // 3. Decode le token
        const {exp: expiration} = jwtDecode(token);
            if(expiration * 10000 > new Date().getTime()) {
                setAxiosToken(token);
                return true;
            }
            return false;
    }
    return false;
}


export default{
    authenticate, 
    logout,
    setup, 
    isConnected
};