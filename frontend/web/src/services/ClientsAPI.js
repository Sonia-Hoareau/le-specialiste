import axios from "axios";
import cache from "./cache";
import { CLIENTS_API } from "../configs/url";

async function findAll() {

    const cacheClients = await cache.get('clients');
    if (cacheClients) return cacheClients;

    return axios
    .get(CLIENTS_API)
    .then(response => {
        const clients =  response.data["hydra:member"];
        cache.set('clients', clients);
        return clients;
    });
}

export default { 
    findAll
};