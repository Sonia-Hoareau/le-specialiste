export const API_URL = 'http://localhost:8000/api/';
export const UPLOAD_URL = 'http://localhost:8000/';

export const BIENS_IMMOBILIERS_API = API_URL + 'bien_immobiliers';
export const ADMINISTRATEURS_API = API_URL + 'administrateurs';
export const CLIENTS_API = API_URL + 'clients';
export const PHOTOS_API = API_URL + 'photos';
export const LOGIN_API = API_URL + 'login_check';