import React from 'react';
import { HashRouter, Switch, Route, Redirect} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'
import BiensImmobiliers from './Sections/BiensImmobiliers';
import BtnAdd from './Sections/Components/BtnAdd';
import Navbar from './Sections/Components/Navbar';
import Logout from '../components/Logout/Logout';
import logoLeSpecialiste from '../logoLeSpecialiste.svg';
import Contacts from '../components/Home/Connexion/SubComponents/Contacts';
import Copyright from '../components/Home/Connexion/SubComponents/Copyright';
import Vendu from './Sections/Vendu';
import Clients from './Sections/Clients';
import BienImmobilier from './Sections/BienImmobilier';
import BienImmobilierLook from './Sections/BienImmobilierLook';


const Base = props =>{

    const logout = () =>{
        props.logout();
    }

//     const [isConnected, setIsConnected] = useState(false);
// console.log(isConnected);
    return(
        <div className="container mx-auto flex">
            {/* partie gauche */ }
            <div className="w-60 bg-bleuSpecialiste h-screen fixed">
                <h2 className="text-4xl pl-10 pt-10 text-white font-semibold mb-6">ADMIN</h2>
                <BtnAdd/>
                <Navbar/>
                {/* <Navbar isConnected={isConnected}/> */}
            </div>

            {/* partie droite */}
            <div className="w-full pr-20 pl-20 ml-21">
                <div className="mt-4 flex justify-between">
                    <p><img src={logoLeSpecialiste} className="h-14" alt="logo" /></p>
                    <div className="text-right">
                        <div className="flex items-center">
                            <p>Bonjour Laura Torn</p>
                            {/* <p/>{username.value}</p> */}
                            <span className="text-4xl text-gray-700 ml-2"><FontAwesomeIcon icon={ faUserCircle } /></span>
                        </div>
                        <Logout logout={() => logout() }/>
                    </div>
                </div>
                
                    <HashRouter>
                        <Switch>
                            {/* <Route exact path="/admin"><Redirect to="/admin/biens_immobiliers" onConnexion={setIsConnected}/></Route> */}
                            <Route exact path="/admin"><Redirect to="/admin/biens_immobiliers" /></Route>
                            <Route path="/admin/biens_immobiliers" component={BiensImmobiliers}></Route>
                            <Route path="/admin/vendu" component={Vendu}></Route>
                            <Route path="/admin/clients" component={Clients}></Route>
                            <Route path="/admin/bien_immobiliers/new" component={BienImmobilier}></Route>
                            <Route path="/admin/bien_immobiliers/:id" component={BienImmobilier}></Route>
                            <Route path="/admin/bien_immobiliers/look" component={BienImmobilierLook}></Route>
                            
                        </Switch>
                    </HashRouter>
                
                {/* <div className="mt-20 overflow-y-scroll ">
                    <BiensImmobiliers/>
                </div> */}

                <Copyright/>
                <Contacts/>
            </div>
            
        </div>   
    );
}
    

export default Base