import React,{ useEffect, useState } from "react";
import Field from "./Components/forms/Fields";
import Select from "./Components/forms/Select";
import TextArea from "./Components/forms/TextArea";
import BiensImmobiliersAPI from "../../services/BiensImmobiliersAPI";
import ClientsAPI from "../../services/ClientsAPI";
import BtnSubmit from "./Components/BtnSubmit";
import BtnEdit from "./Components/BtnEdit";
import { UPLOAD_URL } from "../../configs/url"
import { toast } from "react-toastify";
import FormContentLoader from "../../components/loaders/FormContentLoader";
// import Cache from "../../services/cache";

// Fonction qui permet de formater la date
const formatDate = (date) => {
    const dateObj = new Date(date);
    const month = String(dateObj.getMonth() + 1).padStart(2, '0');
    const day = String(dateObj.getDate()).padStart(2, '0');
    const year = dateObj.getUTCFullYear();
    
    return day + "/" + month + "/" + year;
}

const BienImmobilier = ({ history, match }) => {

    // id = new
    const {id = "new"} = match.params;

    const [bienImmobilier, setBienImmobilier] = useState({
        date: "",
        reference: "",
        type: "",
        yearConstruct: "",
        surface: "",
        numberBedroom: "",
        numberRoom: "",
        surfacePlane: "",
        place: "",
        cp: "",
        city: "",
        // tel: "",
        // email: "",
        dpe: "",
        ges: "",
        price: "",
        description: "",
        images: [], 
        client: "",
    });
    const [errors, setErrors] = useState({
        date: "",
        reference: "",
        type: "",
        yearConstruct: "",
        numberBedroom: "",
        numberRoom: "",
        surface: "",
        surfacePlane: "",
        place: "",
        cp: "",
        city: "",
        // tel: "",
        // email: "",
        dpe: "",
        ges: "",
        price: "",
        description: "",
        images: [], 
        client:""
    });

    // loadin d'attente de chargement de la fiche de création d'un bien immobilier
    const [loading, setLoading] = useState(true);

    // // State pour modifier ou créer un bien immobillier
    const [editing, setEditing] = useState(false);

    // Récupération des biens Immobiliers
    const fetchBienImmobilier = async id => {
        try{
            const { date , reference, type, yearConstruct, surface, numberBedroom, numberRoom, surfacePlane, place, cp, city, tel, email, dpe, ges, price, description, images, client } = await BiensImmobiliersAPI.find(id); 
            setBienImmobilier({ date, reference, type, yearConstruct, surface, numberBedroom, numberRoom, surfacePlane, place, cp, city, tel, email, dpe, ges, price, description,images,  client: client.id });
            // quand les clients sont chargés, on met le loading à false
            setLoading(false);
            
        }catch(error){
            console.log(error.response);
            toast.error("Une erreur est survenue");
            // TODO : notifiction erreur
            history.replace("/admin/biens_immobiliers");
        }
    };    
    // réupération du bien immobillier quand l'identifiant de l'url change
    useEffect(() => {
        
        if (match.params.id) {
            setEditing(true);
            fetchBienImmobilier(match.params.id);
        }else{
            setEditing(false);
            setBienImmobilier({
            date: "",
            reference: "",
            type: "",
            yearConstruct: "",
            surface: "",
            numberBedroom: "",
            numberRoom: "",
            surfacePlane: "",
            place: "",
            cp: "",
            city: "",
            dpe: "",
            ges: "",
            price: "",
            description: "",
            images:[], 
            client: "",});
        }
        
       
    }, [match.params.id]);

    
    // Récupération des clients pour le select
    const [clients, setClients] = useState([]);
    
    const fetchClients = async () => {
        try {
            const data = await ClientsAPI.findAll();
            setClients(data);
            setLoading(false);
        } catch (error) {
            history.replace("/admin/biens_immobiliers");
            toast.error("Une erreur est survenue");
            // TODO : notifiction erreur
            
        }
    };
    // Récupération des clients à chaque chargement du composant
    useEffect(() => {
        fetchClients();
    }, []);

    
    // Gestion du changement des inputs dans le formulaire
    const handleChange = ({ currentTarget }) => {
        const { name, value } = currentTarget;
        setBienImmobilier({ ...bienImmobilier, [name]: value });
    };

    // Gestion du changement des inputs dans le formulaire
    const handleFileChange = ( event ) => {
        const name = event.currentTarget.name;
        // setBienImmobilier({ ...bienImmobilier, photos: "salut"});
        setBienImmobilier({ ...bienImmobilier, [name]: event.target.files[0] });

    };

    // Gestion de la soumission du formulaire
    const handleSubmit = async event => {
        event.preventDefault();
        // console.log(bienImmobilier);
        try {

            // Si l'utilisateur veut modifier un bien immobilier
            if (editing) {
                await BiensImmobiliersAPI.update(id, bienImmobilier);
                toast.success("Le bien immobilier a bien été modifié");
                // TODO : notifiction erreur
                
            // sinon il veut créer un bien immobilier        
            } else {
                await BiensImmobiliersAPI.create(bienImmobilier);
                // Retourner sur la page des biens immobiliers
                toast.success("Le bien immobilier a bien été créé");
                history.replace("/admin/biens_immobiliers");  
                // TODO : notifiction erreur
            }
           
        } catch (error) {
            console.log(error.response);
            toast.error("Une erreur est survenue");
        }
    };

    // const photo = () => {
    //     const [photo, setPhoto] = useState([]);
    
    //     useEffect(() => {
    //         PhotosAPI.findAll()
    //         .then(response => {
    //             setPhotos(response.data);
    //         })
    //         .catch(error => console.log(error.response));
    // }, []);
    // console.log(photos);

    return (
        
        <div className="mt-8 ">
          
            {!editing && <h2 className="uppercase font-semibold text-2xl text-bleuSpecialiste">Ajouter un bien</h2> || <h2 className="uppercase font-semibold text-2xl text-bleuSpecialiste">Modifier un bien</h2> }
            <div className="mt-6">
            {loading && <FormContentLoader/>}
    
                {!loading && <form onSubmit={handleSubmit}>       
                    
                    {/* {!editing && <p className="text-sm flex justify">* champs obligatoires</p>}   */}

                    <div className="flex justify-end my-6 items-center">
                        
                        

                        <Select name="client" label="Client *" value={bienImmobilier.client} onChange={handleChange} error={errors.client}>
                            {/* <option value="">Nom du client</option> */}
                            <option value="">Sélectionner un client</option>
                                {clients.map(client => (
                                    <option key={client.id} value={client.id}>
                                        {client.name}
                                    </option>
                                ))}
                        </Select>
                    
                        
                    </div>

                    <div className="flex ">
                        <div className="flex flex-col w-1/5 mr-4 border p-5">
                        {!editing && <Field name="date" type="date" placeholder="Date" label="Date *" value={bienImmobilier.date} error={errors.date} onChange={handleChange} /> || "" }
                            <Field name="reference" type="text" placeholder="" label="Réf" value={bienImmobilier.reference} error={errors.reference} onChange={handleChange}/>
                            <Select className="pl-4" name="type" label="Type *" value={bienImmobilier.type} onChange={handleChange}>
                                <option value="">Type de bien</option>
                                <option value="Appartement">Appartement</option>
                                <option value="Local">Local</option>
                                <option value="Maison">Maison</option>
                                <option value="Terrain">Terrain</option>
                                
                            </Select>
                            <Field name="yearConstruct" type="text" placeholder="" label="Année de construction" value={bienImmobilier.yearConstruct} error={errors.yearConstruct} onChange={handleChange}/>
                            <Select  name="numberRoom" label="Pièce(s)" value={bienImmobilier.numberRoom} onChange={handleChange}>
                                <option value="">Nombre de pièces</option>
                                <option value="F1">F1</option>
                                <option value="F2">F2</option>
                                <option value="F3">F3</option>
                                <option value="F4">F4</option>
                                <option value="F5">F5</option>
                                <option value="F6">F6</option>
                                <option value="F7">F7</option>
                            </Select>
                            <Field name="surface" type="int" placeholder="" label="Surface habitable (M2)" value={bienImmobilier.surface} error={errors.surface} onChange={handleChange}/> 
                            <Field name="numberBedroom" type="int" placeholder="" label="Chambre(s)" value={bienImmobilier.numberBedroom} error={errors.numberBedroom} onChange={handleChange}/>
                            <Field name="surfacePlane" type="int" placeholder="" label="Surface terrain (M2) *" value={bienImmobilier.surfacePlane} error={errors.surfacePlane} onChange={handleChange}/>
                        </div>

                        <div className="flex flex-col w-3/4">

                            <div className=" border p-5">

                                <div className="flex">
                                    
                                    <div className="flex flex-col mr-8 flex-1"><Field name="place" type="text" placeholder="" label="Adresse *" value={bienImmobilier.place} error={errors.place} onChange={handleChange}/></div>
                                    <div className="flex  mr-8 "><Field name="cp" type="text" placeholder="" label="CP *" value={bienImmobilier.cp} error={errors.cp} onChange={handleChange}/></div>
                                </div>

                                <div className="flex mt-2">
                                    <div className="flex flex-col mr-8 flex-1"><Field name="city" type="text" placeholder="" label="Ville *" value={bienImmobilier.city} error={errors.city} onChange={handleChange}/></div>
                                    {/* <div className="flex  mr-8 "><Field name="tel" type="text" placeholder="" label="Téléphone" value={bienImmobilier.tel} error={errors.tel} onChange={handleChange}/></div> */}
                                </div>

                                {/* <div className="flex mt-2">
                                    <div className="flex  mr-8"><Field name="email" type="text" placeholder="" label="Email" value={bienImmobilier.email} error={errors.email} onChange={handleChange}/></div>
                                </div> */}

                            </div>

                            <div className="flex">
                                <div className="mt-2 border flex p-5 mt-4 w-4/6">
                                    <div className="mr-8"><Field name="dpe" type="int" placeholder="" label="DPE - kWh/m²/an *" value={bienImmobilier.dpe} error={errors.dpe} onChange={handleChange}/></div>
                                    <div className="mr-8"><Field name="ges" type="int" placeholder="" label="GES - kgéqCO2/m².an *" value={bienImmobilier.ges} error={errors.ges} onChange={handleChange}/></div>
                                </div>

                                <div className="flex mt-2 ml-4 border p-5 mt-4 w-3/6">

                                    <div className="w-full mr-8"><Field name="price" type="int" placeholder="" label="Prix - € *" value={bienImmobilier.price} error={errors.price} onChange={handleChange}/></div>                                
                                </div>
                            </div>
                            <div className="flex mt-2">
                            <TextArea name="description" label="Description *" value={bienImmobilier.description} error={errors.description} onChange={handleChange}/>
                            </div>
                            {/* <textarea name="description" className="mt-2" placeholder="" value={bienImmobilier.description} error={errors.description} onChange={handleChange}/> */}
                        
                        
                        </div>

                    </div>

                    <div className="flex flex-col mt-6">

                        {/* Fonction à implémenter en phase 2 */}
                            {/* <button> + Plus de photos</button> */}
                            {/* {bienImmobilier.photos}
                        {Photos.map(photo => (
                            <option key={photo.id} value={photo.id}>
                                {photo.url}
                            </option>
                        ))} */}

                    <div className="upload-image flex flex-wrap">
                        {bienImmobilier.images.map(photo => <img src={UPLOAD_URL + "uploads/images/" + photo.filePath} className="pr-5 py-5" />)}
                    </div>

                    {/* {bienImmobilier.images.map(photo => <img src={API_URL + "public/uploads/images/" + photo.filePath} />)} */}
                    {/* {bienImmobilier.images.map(photo => <img src={"public/uploads/images/" + photo.filePath} />)} */}
                        <div className="flex flex-wrap">
                        <div className="flex flex-col mr-8"><Field name="photos" type="file" placeholder="" label="Télécharger des photos"  onChange={handleFileChange}/></div>
                            {/* <div className="flex flex-col mr-8"><Field name="photos" type="file" placeholder="" label="Télécharger des photos" value={bienImmobilier.photos} onChange={handleChange}/></div>
                            <div className="flex flex-col mr-8"><Field name="photos" type="file" placeholder="" label="Télécharger des photos" value={bienImmobilier.photos} onChange={handleChange}/></div>
                            <div className="flex flex-col mr-8"><Field name="photos" type="file" placeholder="" label="Télécharger des photos" value={bienImmobilier.photos} onChange={handleChange}/></div> */}
                        </div>
                    </div>
                    
                    
                    {!editing && <BtnSubmit/> || <BtnEdit/> }
                    
                    
                </form>}

            </div>
               

              
        </div>
    );
};

export default BienImmobilier
