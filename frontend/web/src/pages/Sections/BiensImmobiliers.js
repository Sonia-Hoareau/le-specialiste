import { faPencilAlt, faSearch, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../../components/loaders/TableLoader';
import BiensImmobiliersAPI from '../../services/BiensImmobiliersAPI';
import Pagination from './Components/Pagination';


// Fonction qui permet de formater la date
const formatDate = (date) => {
    const dateObj = new Date(date);
    const month = String(dateObj.getMonth() + 1).padStart(2, '0');
    const day = String(dateObj.getDate()).padStart(2, '0');
    const year = dateObj.getUTCFullYear();
    
    return day + "/" + month + "/" + year;
}

// Fonction qui permet de formater le prix
const formatPrice = (price) => {
    return new Intl.NumberFormat('fr-FR', {
        style: 'currency',
        currency: 'EUR'
    }).format(price);
}

const BiensImmobiliers = () =>{
    
    const [biensImmobiliers, setBiensImmobiliers] = useState([]);
    // modifier la page courante
    const [currentPage, setCurrentPage] = useState(1);
    // loader d'attente de chargement des biens immobiliers
    const [loading, setLoading] = useState(true);

    // fonction qui va définir la pagination
    const handlePageChange = (page) => {
        setCurrentPage(page);
    }
    const itemsPerPage = 10;
    const paginatedBiensImmobiliers = Pagination.getData(biensImmobiliers, currentPage, itemsPerPage);

    // requête pour récupérer les biens immobiliers
    const fetchBiensImmobiliers = async () => {
        try {
            const data = await BiensImmobiliersAPI.findAll();
            setBiensImmobiliers(data);
            setLoading(false);
        } catch (error) {
            toast.error("Erreur lors du chargement des biens immobiliers");
            console.log(error.response);
        }
    }
    // récupérer les biens immobiliers au chargement du composant
    useEffect(() => fetchBiensImmobiliers(), []);

    // Gestion de la suppression d'un biens immobiliers
    const handleDelete = async (id) => {
        // copie du tableau des biens immobiliers
        const originalBiensImmobiliers = [...biensImmobiliers];

        // 1. Supprimer un bien immobilier de l'affichage
        setBiensImmobiliers(biensImmobiliers.filter(bienImmobilier => bienImmobilier.id !== id))

        // 2. Supprimer un bien immobilier de l'api 
        // requete pour supprimer un bien immobilier
        
        try {
            await BiensImmobiliersAPI.delete(id);
            toast.info("Le bien immobilier a bien été supprimé");
        } catch (error) {
            setBiensImmobiliers(originalBiensImmobiliers);
            toast.error("Une erreur est survenue");
        }
    }


    return(
        <>
        <div className="min-h-34 mt-8 overflow-x-auto">
            {!loading && <table className="table-auto">
                <thead className="h-16">
                    <tr className="uppercase text-left text-bleuSpecialiste ">
                        <th></th>
                        <th className="w-1/12">Date</th>
                        <th className="w-2/12 px-6">Réf</th>
                        <th className="w-2/12">Type</th>
                        <th className="w-1/12 px-6">Pièce(s)</th>
                        <th className="w-3/12">Localisation</th>
                        <th className="w-2/12 px-6">Prix</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {paginatedBiensImmobiliers.map(bienImmobilier =>
                        <tr className="text-bleuSpecialiste h-12" key={bienImmobilier.id}>
                            <td className='px-4'><Link to={"/admin/bien_immobiliers/look"}><span className="text-2xl ml-2 text-gray-300 hover:text-gray-600"><FontAwesomeIcon icon={ faSearch } /></span></Link></td>
                            <td>{formatDate(bienImmobilier.date)}</td>
                            <td className="px-6">{bienImmobilier.reference}</td>
                            <td className="font-semibold">{bienImmobilier.type}</td>
                            <td className="px-6 font-semibold">{bienImmobilier.numberRoom}</td>
                            <td className="font-semibold">{bienImmobilier.city}</td>
                            <td className='px-6'>{formatPrice(bienImmobilier.price)}</td>
                            <td className='px-2'><Link to={"/admin/bien_immobiliers/" + bienImmobilier.id} ><span className="text-2xl ml-2 text-gray-300 hover:text-gray-600"><FontAwesomeIcon icon={ faPencilAlt } /></span></Link></td>
                            <td className='px-4'><button onClick={() => handleDelete(bienImmobilier.id)}><span className="text-2xl ml-2 text-gray-300 hover:text-gray-600"><FontAwesomeIcon icon={ faTrashAlt } /></span></button></td>
                            
                        </tr>
                    )}
                    
                </tbody>
            </table>}
            {loading &&<TableLoader/>}
        </div>
        
        {/* Pagination */}
        <Pagination currentPage={currentPage} itemsPerPage={itemsPerPage} length={biensImmobiliers.length} onPageChanged={handlePageChange}/>

        </>
    );
}

export default BiensImmobiliers