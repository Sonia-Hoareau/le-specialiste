import React from "react";

const TextArea = ({ 
    name,
    label,
    value,
    onChange,
    placeholder,
    
}) => (
   
        <div className="flex flex-col flex-1">
            <label htmlFor={name} className="mr-6 text-blue-800">{label}</label>
            <textarea c
            name={name}
            className="mt-2 py-1 px-2" 
            placeholder={placeholder}
            value={value}
            onChange={onChange}/>
        </div>
);


export default TextArea;