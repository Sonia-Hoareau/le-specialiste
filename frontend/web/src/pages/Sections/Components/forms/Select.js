import React from "react";
import "tailwindcss/tailwind.css";

const Select = ({ 
    name,
    label,
    value,
    onChange,
    placeholder,
    error,
    type = "text",
    children
}) => (
    <>
        <label htmlFor={name} className="mr-6 mt-2 text-blue-800">{label}</label>
        <select
            className={"mt-2 py-1 px-2" + (error && "text-red-500")}
            type={type}
            id={name}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
        >
            {children}
        </select>
        <p className="text-red-500 text-xs italic">{error}</p>
    </>
);


export default Select;