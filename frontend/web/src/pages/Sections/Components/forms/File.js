import React from "react";

const File = ({ 
    name,
    label,
    value,
    onChange,
    placeholder,
    type = "text",
    error = ""
}) => (
   
    <div className="mt-4">
        <label htmlFor={name} className="text-blue-800">{label}</label>
        <input
            className="mt-1 py-1 px-2 bg-blue-100 w-full"
            type={type}
            id={name}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
        />
        {error && <p className="invalid-feedback">{error}</p>}
    </div>
);


export default File;