import React from 'react'
import { Link } from 'react-router-dom';

const BtnEdit = ( ) =>{
    return(
        <div className="flex justify-end mt-10 ">
            <Link  to={"/admin/biens_immobiliers/"} className="bg-gray-700 hover:bg-gray-900 text-white px-6 py-2 w-50 mr-8 font-semibold">
                Retour à la liste
            </Link>   

            <button type="submit" className="bg-blue-800 hover:bg-green-400 hover:text-gray-700 text-white px-6 py-2 w-36 font-semibold ">
                Modifier
            </button> 
        </div>
        
    )
}


export default BtnEdit