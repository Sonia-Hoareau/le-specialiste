import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = props =>{
    return(

        <nav className="flex flex-col pt-10">
            <NavLink to="/admin/biens_immobiliers" className="font-semibold pl-10 bg-bleuLightSpecialiste active:bg-rougeSpecialiste hover:bg-rougeSpecialiste px-4 py-2 text-white text-lg mb-1">Tous les biens</NavLink>
            <NavLink to="/admin/vendu" className="font-semibold pl-10 bg-bleuLightSpecialiste hover:bg-rougeSpecialiste px-4 py-2 text-white text-lg mb-1">Vendu</NavLink>
            <NavLink to="/admin/clients" className="font-semibold pl-10 bg-bleuLightSpecialiste hover:bg-rougeSpecialiste px-4 py-2 text-white text-lg mb-1">Clients</NavLink>
            
        </nav>
    )
}

export default Navbar