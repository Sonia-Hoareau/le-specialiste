import React, { useEffect, useState, useReducer } from "react";

const Pagination = ({length, biensImmobiliers,currentPage, onPageChanged, itemsPerPage}) => {
    const [pagesCount, setpagesCount] = useState(1);
    const [pages, setpages] = useState([]);
  
    useEffect(() => {
        setpagesCount(Math.ceil(length / itemsPerPage));

        let test = [];
        for(let i=1; i<=pagesCount; i++){
               test.push(i);  
            }  
            setpages(test);   
    }, [biensImmobiliers, length, pagesCount]);

    
    return(
        <div className="pagination text-gray-500 mt-10">
            <ul>
                <div className="flex justify-between">
                <li className="flex-grow px-5 text-left">
                    <button onClick={() => onPageChanged(currentPage - 1)} id="precedant" disabled={(currentPage === 1)}>Précédant</button>
                </li>
                    {pages.map(page =>
                        <li key={page} className="flex-grow-0 px-5">
                            <button className={(currentPage === page && "text-bleuSpecialiste font-bold border-bleuSpecialiste")} onClick={() => onPageChanged(page)}>{page}</button>
                        </li>
                    )}
                    <li className="flex-grow px-5 text-right">
                        <button onClick={() => onPageChanged(currentPage + 1)} id="suivant" className="" disabled={(currentPage === pagesCount)}>Suivant</button>
                    </li>
                </div>     
            </ul>

        </div>
    );
}

Pagination.getData = (items, currentPage, itemsPerPage) => {
    const start = (currentPage - 1) * itemsPerPage;
    return items.slice( start, start + itemsPerPage);
}

export default Pagination;