import React from 'react'

const BtnSubmit = ( ) =>{
    return(
        <div className="flex justify-end mt-10 ">
            <button type="submit"className="bg-gray-700 hover:bg-gray-900 text-white px-6 py-2 w-36 mr-8 font-semibold">
                Annuler
            </button>   

            <button type="submit" className="bg-blue-800 hover:bg-green-400 hover:text-gray-700 text-white px-6 py-2 w-36 font-semibold ">
                Enregistrer
            </button> 
        </div>
        
    )
}

export default BtnSubmit