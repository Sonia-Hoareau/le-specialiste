import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'

const BtnAdd = ( ) =>{
    return(

        <NavLink to="/admin/bien_immobiliers/new" className=" items-center ml-8 mt-5 bg-bleuCielSpecialiste hover:bg-green-400 hover:text-gray-700 text-blue-800 hover:text-white text-lg text-center px-3 py-2">
            <span className="text-1xl mr-3"><FontAwesomeIcon icon={ faPlus }/></span>
            <span className="font-semibold text-bleuSpecialiste hover:text-gray-700">Ajouter un bien</span>
        </NavLink>    
    )
}

export default BtnAdd