const fields = {
    inputs:[
        {
            name: 'date',
            placeholder: 'date',
            type: 'date',
            value: 'date'
        },
        {
            name: 'reference',
            placeholder: 'régérence',
            type: 'text',
            value: 'reference'
        },
        {
            name: 'type',
            placeholder: 'type',
            type: 'text',
            value: 'type'
        },
        {
            name: 'year_construct',
            placeholder: 'Année de construction',
            type: 'number',
            value: 'year_construct'
        },
        {
            name: 'number_room',
            placeholder: 'Nombre de pièces',
            type: 'text',
            value: 'number_room'
        },
        {
            name: 'surface',
            placeholder: 'M2',
            type: 'number',
            value: 'surface'
        },
        {
            name: 'number_bedroom',
            placeholder: 'Nombre de chambres',
            type: 'number',
            value: 'number_bedroom',
        },
        {
            name: 'surface_plane',
            placeholder: 'surface terrain',
            type: 'number',
            value: 'surface_plane',
        },
        {
            name: 'place',
            placeholder: 'adresse',
            type: 'text',
            value: 'place',
        },
        {
            name:'cp',
            placeholder: 'code postal',
            type: 'text',
            value: 'cp'
        },
        {
            name: 'city',
            placeholder: 'ville',
            type: 'text',
            value: 'city',
        },
        {
            name: 'tel',
            placeholder: 'téléphone',
            type: 'text',
            value: 'tel',
        },
        {
            name: 'email',
            placeholder: 'email',
            type: 'text',
            value: 'email',
        },
        {
            name: 'dpe',
            placeholder: 'DPE',
            type: 'int',
            value: 'dpe',
        },
        {
            name: 'ges',
            placeholder: 'GES',
            type: 'int',
            value: 'ges',
        },
        {
            name: 'price',
            placeholder: 'prix',
            type: 'number',
            value: 'price',
        },
        {
            name: 'description',
            placeholder: 'description',
            type: 'text',
            value: 'description'
        },
        {
            name: 'photos',
            placeholder: 'photos',
            type: 'url',
            value: 'photos'
        }
    ]
};

export default fields;