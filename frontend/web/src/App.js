import React , {useState} from 'react';
import './index.css';
import "tailwindcss/tailwind.css";
import Home from './components/Home/Home';
import Base from './pages/Base';
import { HashRouter, Switch, Route, Redirect} from 'react-router-dom';
import AuthAPI from './services/AuthAPI';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    
const [isConnected, setIsConnected] = useState(
    AuthAPI.isConnected()
);
    return (  
        <>
        <HashRouter>
            <Switch >
            <Redirect exact from="/" to="/home" />
            {
                isConnected ?
                <Redirect exact from="/home" to="/admin" />:<Redirect from="/admin" to="/home" />
            }
                <Route 
                    exact path="/home" render={props => (
                        <Home
                             isConnected={isConnected} login={() => setIsConnected(true)}
                        />
                    )}
                />  
                <Route path="/admin" render={props => (
                        <Base
                           logout={() => setIsConnected(false)}
                        />
                    )}
                />    
            </Switch>   
        </HashRouter>
        <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
        </>
    );
}

 export default App;
