import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Acheter from './Components/Acheter'
import Vendre from './Components/Vendre'
import tailwind from 'tailwind-rn';


export default function App() {
  return (
    <View style={styles.container}>
      <Text style={tailwind('text-2xl font-semibold w-3/4 text-center')}>Vous souhaitez acheter un bien immobilier,</Text>
      <Text style={tailwind('text-2xl font-semibold')}>3 étapes</Text>
      <Text style={tailwind('text-lg w-1/2 leading-6 pt-2.5 pb-2.5')}>une recherche simple, vous nous contactez, et on s'occupe de vous !</Text>
      
      <View style={styles.container2}>
        <Acheter/>
        <Vendre/>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignContent: "center",
    justifyContent: 'center',
    margin: 15
  },
  container2: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    
  }
});

