import React from 'react'
import { View, Button, StyleSheet } from 'react-native'
import tailwind from 'tailwind-rn';

class Vendre extends React.Component{
    render(){
        return(
            <View style={ styles.containerButton }>
                <Button style={tailwind('lowercase')} color= "#004F8A" title="Vendre" />
            </View>
            
            
        )
    
    }
   
}


const styles = StyleSheet.create({
    containerButton: {
        width: 162,
        marginLeft:2, 
        height: 40, 
        fontSize: 20,
        backgroundColor: '#BEF6FF'
    }
})

export default Vendre