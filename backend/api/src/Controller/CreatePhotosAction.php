<?php

namespace App\Controller;

use App\Entity\Photos;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Repository\BienImmobilierRepository;

final class CreatePhotosAction
{
    private $bienImmobilierRepository;

    public function __construct(BienImmobilierRepository $bienImmobilierRepository)
    {

        $this->bienImmobilierRepository = $bienImmobilierRepository;
    }


    public function __invoke(Request $request): Photos
    {
        
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
 
        $photos = new Photos();
        $photos->file = $uploadedFile;
        $bienImmobilier = $this->bienImmobilierRepository
        ->findOneBy(['id' => $request->request
        ->getInt('bienImmobilier')]);
        $photos->setBienImmobilier($bienImmobilier);

        return $photos;
    }
}
