<?php

namespace App\Entity;

use App\Repository\BienImmobilierRepository;
use App\Entity\Photos;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BienImmobilierRepository::class)
 * @ApiResource(normalizationContext={"groups"={"bienImmobiliers_read"}},denormalizationContext={"groups"={"bienImmobiliers_write"}},iri="http://schema.org/BienImmobilier")
 */

class BienImmobilier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $reference;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $surface;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $place;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $yearConstruct;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $price;

    /**
     * @ORM\Column(type="date")
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */

    private $date;

    /**
     * @ORM\Column(type="integer")
     *@groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $dpe;

    /**
     * @ORM\Column(type="integer")
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $ges;

    /**
     * @ORM\Column(type="integer")
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $surfacePlane;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="bienImmobiliers")
     * @ORM\JoinColumn(nullable=false)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $client;

    /**
     * @var Photos|null
     * @ORM\OneToMany(targetEntity=Photos::class, mappedBy="bienImmobilier", cascade={"remove"})
     * @groups({"bienImmobiliers_read"})
     */
    private $images;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\JoinColumn(nullable=true)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     * @ApiProperty(iri="http://schema.org/image")
     */
    private $numberBedroom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $numberRoom;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"bienImmobiliers_read", "bienImmobiliers_write"})
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSurface(): ?int
    {
        return $this->surface;
    }

    public function setSurface(?int $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getYearConstruct(): ?int
    {
        return $this->yearConstruct;
    }

    public function setYearConstruct(?int $yearConstruct): self
    {
        $this->yearConstruct = $yearConstruct;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDpe(): ?int
    {
        return $this->dpe;
    }

    public function setDpe(int $dpe): self
    {
        $this->dpe = $dpe;

        return $this;
    }

    public function getGes(): ?int
    {
        return $this->ges;
    }

    public function setGes(int $ges): self
    {
        $this->ges = $ges;

        return $this;
    }

    public function getSurfacePlane(): ?int
    {
        return $this->surfacePlane;
    }

    public function setSurfacePlane(int $surfacePlane): self
    {
        $this->surfacePlane = $surfacePlane;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Photos[]
     */
    public function getImages()
    {
        return $this->images;
    }

    public function addImage(Photos $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setBienImmobilier($this);
        }
        return $this;
    }

    public function removeImage(Photos $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getBienImmobilier() === $this) {
                $image->setBienImmobilier(null);
            }
        }
        return $this;
    }

    public function getNumberBedroom(): ?int
    {
        return $this->numberBedroom;
    }

    public function setNumberBedroom(?int $numberBedroom): self
    {
        $this->numberBedroom = $numberBedroom;

        return $this;
    }

    public function getNumberRoom(): ?string
    {
        return $this->numberRoom;
    }

    public function setNumberRoom(?string $numberRoom): self
    {
        $this->numberRoom = $numberRoom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
