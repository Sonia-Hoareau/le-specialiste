<?php

namespace App\Entity;

use App\Repository\PhotosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CreatePhotosAction;

/**
 * @ORM\Entity(repositoryClass=PhotosRepository::class)
 * @ApiResource(
 *     iri="http://schema.org/Photos",
 *     normalizationContext={
 *         "groups"={"photos_read"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreatePhotosAction::class,
 *             "deserialize"=false,
 *             "security"="is_granted('ROLE_USER')",
 *             "validation_groups"={"Default", "photos_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     },
 *                                      "bienImmobilier"={
 *                                         "type"="int"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get",
 *     },
 *     itemOperations={
 *         "get",
 *        "delete"={
 *            "security"="is_granted('ROLE_USER')",
 *           "security_message"="Accès refusé!"
 *       }
 *     }
 * )
 * @Vich\Uploadable
 */

class Photos
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    // /**
    //  * @ORM\Column(type="blob", nullable=true)
    //  */
    // private $photo_blob;

    /**
     * @ORM\OneToMany(targetEntity=BienImmobilier::class, mappedBy="images")
     * cascade={"remove"}
     * @Groups({"photos_read"})
     */
    private $photo_id;

    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"photos_read", "bienImmobiliers_read"})
     */
    public $contentUrl;

     /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"photos_create"})
     * @Vich\UploadableField(mapping="bienImmobilier_images", fileNameProperty="filePath")
     */
    public $file;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     * @Groups({"photos_read", "bienImmobiliers_read"})
     */
    public $filePath;

    /**
     * @ORM\ManyToOne(targetEntity=BienImmobilier::class, inversedBy="images")
     */
    private $bienImmobilier;

    public function __construct()
    {
        $this->photo_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBienImmobilier(): ?BienImmobilier
    {
        return $this->bienImmobilier;
    }

    public function setBienImmobilier(?BienImmobilier $bienImmobilier): self
    {
        $this->bienImmobilier = $bienImmobilier;

        return $this;
    }

}
