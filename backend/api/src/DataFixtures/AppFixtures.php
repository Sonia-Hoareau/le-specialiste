<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Client;
use App\Entity\Photos;
use App\Entity\NumberRooms;
use App\Entity\Administrateur;
use App\Entity\BienImmobilier;
use App\Entity\NumberBedrooms;
use App\Entity\Type;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use PhpCsFixer\Fixer\Alias\RandomApiMigrationFixer;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * L'encodeur de mot de passe
     *
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // use the factory to create a Faker\Generator instance
        $faker = Factory::create();

        // Création d'un administrateur
        for ($a = 0; $a < 3; $a++) {
            $administrateur = new Administrateur();
            $administrateur->setFirstname($faker->firstname);
            $administrateur->setLastname($faker->lastname);
            $administrateur->setEmail($faker->email);
            $administrateur->setPassword($this->encoder->encodePassword($administrateur, 'password'));
            $manager->persist($administrateur);

            // Création de clients
            for ($c = 0; $c < mt_rand(1, 20); $c++) {
                $client = new Client();
                $client->setCivility($faker->randomElement(['Monsieur', 'Madame']));
                $client->setFirstname($faker->firstname);
                $client->setName($faker->name);
                $client->setTel($faker->randomNumber(8));
                $client->setEmail($faker->email);
                $client->setAdministrateur($administrateur);
                $manager->persist($client);

                // Création de biens Immobiliers pour chaque client
                for ($b = 0; $b < mt_rand(1, 5); $b++) {
                    $bienImmobilier = new BienImmobilier();
                    $bienImmobilier->setReference($faker->bothify('???-#####'));
                    $bienImmobilier->setSurface($faker->numberBetween(50, 500));
                    $bienImmobilier->setPlace($faker->address('fr_FR'));
                    $bienImmobilier->setcp($faker->postcode);
                    $bienImmobilier->setcity($faker->city);
                    $bienImmobilier->setYearConstruct($faker->year);
                    $bienImmobilier->setDescription($faker->text(200));
                    $bienImmobilier->setPrice($faker->numberBetween(0, 1000000));
                    $bienImmobilier->setDate($faker->dateTimeBetween('-1 years', 'now'));
                    $bienImmobilier->setDpe($faker->numberBetween(50, 450));
                    $bienImmobilier->setGes($faker->numberBetween(0, 80));
                    $bienImmobilier->setSurfacePlane($faker->numberBetween(0, 5000));
                    $bienImmobilier->setNumberBedroom($faker->randomElement(['1', '2', '3', '4', '5']));
                    $bienImmobilier->setNumberRoom($faker->randomElement(['F1', 'F2', 'F3', 'F4', 'F5', 'F6']));
                    $bienImmobilier->setType($faker->randomElement(['Appartement', 'Maison', 'Terrain', 'Local']));
                    $bienImmobilier->setClient($client);
                    $manager->persist($bienImmobilier);
                }
            }
        }
        $manager->flush();
    }
}
