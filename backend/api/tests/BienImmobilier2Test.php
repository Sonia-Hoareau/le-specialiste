<?php

// namespace App\Tests;

// use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
// use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;


// class ApiBienImmobilierTest extends ApiTestCase{
//     private $token;
//     private $clientWithCredentials;

   

//     public function setUp(): void
//     {
//         self::bootKernel();
//     }

//     protected function createClientWithCredentials($token = null): Client
//     {
//         $token = $token ?: $this->getToken();

//         return static::createClient([], ['headers' => ['authorization' => 'Bearer '.$token]]);
//     }

//     /**
//      * Use other credentials if needed.
//      */
//     protected function getToken($body = []): string
//     {
//         if ($this->token) {
//             return $this->token;
//         }

//         $response = static::createClient()->request('POST', '/api/login_check', ['body' => $body ?: [
//             'username' => 'oschuster@gmail.com',
//             'password' => 'password',
//         ]]);

//         $this->assertResponseIsSuccessful();
//         $data = json_decode($response->getContent());
//         $this->token = $data->access_token;

//         return $data->access_token;
//     }

//     public function testAdminResource()
//     {
//         $response = $this->createClientWithCredentials()->request('GET', '/api/bien_immobiliers');
//         $this->assertResponseIsSuccessful();
//     }

//     public function testLoginAsUser()
//     {
//         $token = $this->getToken([
//             'username' => 'deheklf@gmail.com',
//             'password' => '$fjzmpi',
//         ]);

//         $response = $this->createClientWithCredentials($token)->request('GET', '/api/bien_immobiliers');
//         $this->assertJsonContains(['hydra:description' => 'Access Denied.']);
//         $this->assertResponseStatusCodeSame(403);
//     }

//     public function testPostBienImmobilier()
//     {
//         $response = $this->createClient()->request('POST', '/api/bien-immobiliers', [
//             "reference" => "ADJ 256987",
//             "surface" => 150,
//             "place" => '7, rue du pont',
//             "cp" => "70000",
//             "city" => 'vesoul',
//             "year_construct" => 2021,
//             "description" => "Belle maison neuve de type  T5 avec belle vue sur le parc des anges.",
//             "price" => 152000,
//             "date" => '2021-01-11',
//             "dpe" => 155,
//             "ges" => 12,
//             "surface_plane" => 1200,
//             "client_id" => 15,
//             "number_bedroom" => 4,
//             "number_room" => 'F5',
//             'type' => 'Maison'
//         ]);
//                 $this->assertResponseStatusCodeSame(201);
//                 $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//                 $this->assertJsonContains([
//                     "id"=>140,
//                     "reference" => "ADJ 256987",
//                     "surface" => 150,
//                     "place" => '7, rue du pont'
                    
//                 ]);
//                 $this->assertMatchesResourceItemJsonSchema(BienImmobilier::class);

//             }
// }

