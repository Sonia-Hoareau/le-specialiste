<?php

namespace App\Tests\Entity;

use App\Entity\Administrateur;
use PHPUnit\Framework\TestCase;


class AdministrateurTest extends TestCase
{
    public function testUser(){
        $user = new Administrateur();

        //set lastname
        $user->setLastname('Doe');
        $this->assertEquals('Doe', $user->getLastname());

        //set firstname
        $user->setFirstname('Daniel');
        $this->assertEquals('Daniel', $user->getFirstname());

        //set email
        $user->setEmail('test3@mail.com');
        $this->assertEquals('test3@mail.com', $user->getEmail());

        //set password
        $user->setPassword('password');
        $this->assertEquals('password', $user->getPassword());

        

    }
    
}